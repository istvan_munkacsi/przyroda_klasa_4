package com.wsiiz.projekt.przyroda_klasa_4.fragments.home

import androidx.cardview.widget.CardView
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.ConcatAdapter
import com.wsiiz.projekt.przyroda_klasa_4.base.recycler_view_adapter.BasePostsAdapter
import com.wsiiz.projekt.przyroda_klasa_4.base.recycler_view_adapter.HeaderRecycleViewAdapter
import com.wsiiz.projekt.przyroda_klasa_4.base.view_model.BaseStatefulRecyclerViewViewModel
import com.wsiiz.projekt.przyroda_klasa_4.data.dto.UserPost
import com.wsiiz.projekt.przyroda_klasa_4.data.repository.PostRepository
import io.reactivex.Single

class
HomeViewModel @ViewModelInject constructor(private val postRepository: PostRepository) :
    BaseStatefulRecyclerViewViewModel<HomeViewModel.State>(), BasePostsAdapter.PostInteractionContract {

    sealed class State {
        class OnPostClick(val userPost: UserPost) : State()
        object ErrorLoading : State()
    }

    override val state = MutableLiveData<State>()

    private var adapter: ConcatAdapter? = null

    private fun loadPosts(pageNumber: Int): Single<List<UserPost>> {
        return postRepository.getAll(pageNumber)
    }

    private fun like(postId: Int) {
        postRepository.like(postId)
    }

    fun getAdapter(): ConcatAdapter {
        if (adapter == null) {
            val postsAdapter = BasePostsAdapter(this)
            val headerAdapter = HeaderRecycleViewAdapter()

            adapter = ConcatAdapter(headerAdapter, postsAdapter)
        }

        return adapter!!
    }

    override fun onPostClick(cardView: CardView, userPost: UserPost) {
        state.postValue(State.OnPostClick(userPost))
    }

    override fun onLikeClick(userPost: UserPost) {
        like(userPost.id)
    }

    override fun loadMoreData(pageNumber: Int): Single<List<UserPost>> {
        return loadPosts(pageNumber)
    }

    override fun onErrorLoading(error: Throwable) {
        state.postValue(State.ErrorLoading)
    }
}