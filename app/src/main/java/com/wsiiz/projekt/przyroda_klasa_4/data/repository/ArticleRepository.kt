package com.wsiiz.projekt.przyroda_klasa_4.data.repository

import com.wsiiz.projekt.przyroda_klasa_4.data.dto.Article
import com.wsiiz.projekt.przyroda_klasa_4.data.network.service.ArticleService
import io.reactivex.Single
import javax.inject.Inject

class ArticleRepository @Inject constructor(private val articleService: ArticleService) {

    fun getAll(pageNumber: Int): Single<List<Article>> {
        return articleService.getAll(pageNumber)
    }
}