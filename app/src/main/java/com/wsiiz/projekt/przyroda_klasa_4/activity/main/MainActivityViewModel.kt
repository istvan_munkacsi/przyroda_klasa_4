package com.wsiiz.projekt.przyroda_klasa_4.activity.main

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import com.wsiiz.projekt.przyroda_klasa_4.base.view_model.BaseViewModel

class MainActivityViewModel @ViewModelInject constructor() : BaseViewModel<Unit>() {

    override val state: MutableLiveData<Unit>
        get() = MutableLiveData()
}