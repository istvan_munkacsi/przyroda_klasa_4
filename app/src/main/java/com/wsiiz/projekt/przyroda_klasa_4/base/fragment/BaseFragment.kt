package com.wsiiz.projekt.przyroda_klasa_4.base.fragment

import androidx.fragment.app.Fragment
import com.wsiiz.projekt.przyroda_klasa_4.activity.main.MainActivity
import com.wsiiz.projekt.przyroda_klasa_4.base.view_model.BaseViewModel

abstract class BaseFragment<ViewModel : BaseViewModel<*>> : Fragment() {

    protected abstract val viewModel: ViewModel
    fun mainActivity() = activity as MainActivity

    override fun onDestroyView() {
        super.onDestroyView()
        viewModel.onViewDestroyed()
    }
}