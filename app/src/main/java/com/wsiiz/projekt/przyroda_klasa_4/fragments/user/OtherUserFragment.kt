package com.wsiiz.projekt.przyroda_klasa_4.fragments.user

import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.RecyclerView
import com.wsiiz.projekt.przyroda_klasa_4.R
import com.wsiiz.projekt.przyroda_klasa_4.databinding.OtherUserFragmentLayoutBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class OtherUserFragment : UserFragment<OtherUserFragmentLayoutBinding, OtherUserViewModel>() {

    override val viewModel: OtherUserViewModel by viewModels()

    override fun getLayoutId() = R.layout.other_user_fragment_layout

    override fun getRecyclerView(): RecyclerView {
        return binding.recyclerView
    }
}