package com.wsiiz.projekt.przyroda_klasa_4.data.network.service

import com.wsiiz.projekt.przyroda_klasa_4.data.dto.Article
import com.wsiiz.projekt.przyroda_klasa_4.data.network.Endpoints
import io.reactivex.Single
import retrofit2.http.*

interface ArticleService {

    @GET(Endpoints.GET_ALL_ARTICLES)
    fun getAll(@Query(Endpoints.PAGE_NUMBER_PARAM) pageNumber: Int): Single<List<Article>>
}