package com.wsiiz.projekt.przyroda_klasa_4.base.fragment

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.wsiiz.projekt.przyroda_klasa_4.base.view_model.BaseStatefulRecyclerViewViewModel

abstract class BaseStatefulRecyclerViewFragment<ViewModel : BaseStatefulRecyclerViewViewModel<*>> :
    BaseFragment<ViewModel>() {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getRecyclerView()?.scrollToPosition(viewModel.adapterSavedPosition)
        viewModel.setupRecyclerView(getRecyclerView())
    }

    abstract fun getRecyclerView(): RecyclerView?

    override fun onDestroy() {
        viewModel.onDestroy()
        super.onDestroy()
    }
}