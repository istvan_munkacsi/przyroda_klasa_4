package com.wsiiz.projekt.przyroda_klasa_4.fragments.user

import androidx.cardview.widget.CardView
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.FragmentNavigatorExtras
import androidx.recyclerview.widget.RecyclerView
import com.wsiiz.projekt.przyroda_klasa_4.R
import com.wsiiz.projekt.przyroda_klasa_4.data.dto.Post
import com.wsiiz.projekt.przyroda_klasa_4.data.dto.UserPost
import com.wsiiz.projekt.przyroda_klasa_4.data.shared_preferences.SharedPreferences
import com.wsiiz.projekt.przyroda_klasa_4.databinding.ThisUserFragmentLayoutBinding
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class ThisUserFragment : UserFragment<ThisUserFragmentLayoutBinding, ThisUserViewModel>() {

    @Inject
    lateinit var sharedPreferences: SharedPreferences

    override val viewModel: ThisUserViewModel by viewModels()

    override fun getLayoutId() = R.layout.this_user_fragment_layout

    private fun onPostClick(cardView: CardView, userPost: UserPost) {
        val extras = FragmentNavigatorExtras(
            cardView to "postTransition"
        )

        val direction =
            ThisUserFragmentDirections.actionThisUserFragmentToEditPostFragment(
                Post.map(userPost)
            )
        mainActivity().navigateTo(direction, extras)
    }

    override fun onViewModelStateChanged(state: UserViewModel.State) {
        when (state) {
            is UserViewModel.State.PostClick -> onPostClick(state.cardView, state.userPost)
        }
    }

    override fun getRecyclerView(): RecyclerView {
        return binding.recyclerView
    }
}