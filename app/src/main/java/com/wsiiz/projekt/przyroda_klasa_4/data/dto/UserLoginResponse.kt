package com.wsiiz.projekt.przyroda_klasa_4.data.dto

data class UserLoginResponse(
    val id: Int,
    val name: String?
)