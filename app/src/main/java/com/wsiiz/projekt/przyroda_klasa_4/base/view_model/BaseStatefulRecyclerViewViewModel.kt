package com.wsiiz.projekt.przyroda_klasa_4.base.view_model

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

abstract class BaseStatefulRecyclerViewViewModel<State> : BaseViewModel<State>() {
    var recyclerView: RecyclerView? = null
    var adapterSavedPosition = 0


    fun setupRecyclerView(recyclerView: RecyclerView?) {
        this.recyclerView = recyclerView
    }

    override fun onViewDestroyed() {
        super.onViewDestroyed()
        adapterSavedPosition =
            (recyclerView?.layoutManager as LinearLayoutManager?)?.findLastCompletelyVisibleItemPosition()
                ?: 0
    }

    fun onDestroy() {
       recyclerView = null
    }
}