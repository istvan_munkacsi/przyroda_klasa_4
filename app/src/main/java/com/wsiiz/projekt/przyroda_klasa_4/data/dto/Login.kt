package com.wsiiz.projekt.przyroda_klasa_4.data.dto

data class Login(
    val username: String,
    val password: String
)