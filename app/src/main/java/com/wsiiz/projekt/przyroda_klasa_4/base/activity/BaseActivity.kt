package com.wsiiz.projekt.przyroda_klasa_4.base.activity

import androidx.appcompat.app.AppCompatActivity
import com.wsiiz.projekt.przyroda_klasa_4.base.view_model.BaseViewModel

abstract class BaseActivity<ViewModel : BaseViewModel<*>> : AppCompatActivity() {

    abstract val viewModel: ViewModel

    override fun onDestroy() {
        super.onDestroy()

        viewModel.onViewDestroyed()
    }
}