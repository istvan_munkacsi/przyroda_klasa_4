package com.wsiiz.projekt.przyroda_klasa_4.data.repository

import com.wsiiz.projekt.przyroda_klasa_4.data.dto.Login
import com.wsiiz.projekt.przyroda_klasa_4.data.dto.UserLoginResponse
import com.wsiiz.projekt.przyroda_klasa_4.data.network.service.AuthenticationService
import io.reactivex.Single
import retrofit2.Response
import javax.inject.Inject

class UserRepository @Inject constructor(private val authenticationService: AuthenticationService) {

    fun login(login: Login): Single<Response<UserLoginResponse>> {
        return authenticationService.login(login)
    }
}