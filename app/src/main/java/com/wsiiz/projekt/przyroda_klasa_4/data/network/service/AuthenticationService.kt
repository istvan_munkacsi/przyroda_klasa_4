package com.wsiiz.projekt.przyroda_klasa_4.data.network.service

import com.wsiiz.projekt.przyroda_klasa_4.data.dto.Login
import com.wsiiz.projekt.przyroda_klasa_4.data.dto.UserLoginResponse
import com.wsiiz.projekt.przyroda_klasa_4.data.network.Endpoints
import io.reactivex.Single
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST

interface AuthenticationService {

    @POST(Endpoints.LOGIN)
    fun login(@Body login: Login): Single<Response<UserLoginResponse>>
}