package com.wsiiz.projekt.przyroda_klasa_4.application

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class Application : Application()