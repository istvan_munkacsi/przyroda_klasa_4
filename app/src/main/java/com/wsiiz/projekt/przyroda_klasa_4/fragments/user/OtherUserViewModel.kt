package com.wsiiz.projekt.przyroda_klasa_4.fragments.user

import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.SavedStateHandle
import androidx.recyclerview.widget.ConcatAdapter
import androidx.recyclerview.widget.RecyclerView
import com.wsiiz.projekt.przyroda_klasa_4.base.recycler_view_adapter.BasePostsAdapter
import com.wsiiz.projekt.przyroda_klasa_4.base.recycler_view_adapter.HeaderRecycleViewAdapter
import com.wsiiz.projekt.przyroda_klasa_4.data.dto.UserPost
import com.wsiiz.projekt.przyroda_klasa_4.data.repository.PostRepository
import com.wsiiz.projekt.przyroda_klasa_4.utils.SavedStateHandleHelper.safeArgs

class OtherUserViewModel @ViewModelInject constructor(
    userPostRepository: PostRepository,
    @Assisted private val savedStateHandle: SavedStateHandle
) : UserViewModel(
    safeArgs<OtherUserFragmentArgs>(savedStateHandle).userId,
    userPostRepository
) {

    override fun buildRecycleViewAdapter(): RecyclerView.Adapter<*> {
        val headerAdapter = HeaderRecycleViewAdapter()
        val postsAdapter = BasePostsAdapter(this)

        return ConcatAdapter(headerAdapter, postsAdapter)
    }

    fun getPostsAdapter() = (getRecyclerViewAdapter() as ConcatAdapter).adapters[1] as BasePostsAdapter
}