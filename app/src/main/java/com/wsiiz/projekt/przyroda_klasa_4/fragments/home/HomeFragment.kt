package com.wsiiz.projekt.przyroda_klasa_4.fragments.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.databinding.library.baseAdapters.BR
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView
import com.wsiiz.projekt.przyroda_klasa_4.R
import com.wsiiz.projekt.przyroda_klasa_4.base.fragment.BaseFragment
import com.wsiiz.projekt.przyroda_klasa_4.base.fragment.BaseStatefulRecyclerViewFragment
import com.wsiiz.projekt.przyroda_klasa_4.data.dto.UserPost
import com.wsiiz.projekt.przyroda_klasa_4.data.shared_preferences.SharedPreferences
import com.wsiiz.projekt.przyroda_klasa_4.databinding.HomeFragmentLayoutBinding
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class HomeFragment : BaseStatefulRecyclerViewFragment<HomeViewModel>() {

    @Inject
    lateinit var sharedPreferences: SharedPreferences

    private lateinit var binding: HomeFragmentLayoutBinding
    override val viewModel: HomeViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.home_fragment_layout, container, false)
        binding.lifecycleOwner = this
        binding.setVariable(BR.viewModel, viewModel)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.state.observe(viewLifecycleOwner, Observer {
            when (it) {
                null -> return@Observer
                is HomeViewModel.State.OnPostClick -> {
                    onPostClick(it.userPost)

                }
                is HomeViewModel.State.ErrorLoading -> {
                    onErrorLoading()
                }
            }
        })
    }

    private fun onErrorLoading() {
        val context = context ?: return
        Toast.makeText(context, R.string.error, Toast.LENGTH_SHORT).show()
    }

    private fun onPostClick(userPost: UserPost) {
        if (sharedPreferences.getUserId() == userPost.userId) {
            mainActivity().navigateTo(
                HomeFragmentDirections.actionHomeFragmentToThisUserFragment()
            )
        } else {
            mainActivity().navigateTo(
                HomeFragmentDirections.actionHomeFragmentToOtherUserFragment(userPost.userId)
            )
        }
    }

    override fun getRecyclerView(): RecyclerView {
        return binding.recyclerView
    }
}