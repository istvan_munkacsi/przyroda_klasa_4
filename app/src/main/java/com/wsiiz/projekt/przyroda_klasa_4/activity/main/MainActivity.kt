package com.wsiiz.projekt.przyroda_klasa_4.activity.main

import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil
import androidx.databinding.library.baseAdapters.BR
import androidx.navigation.NavController
import androidx.navigation.NavDirections
import androidx.navigation.fragment.FragmentNavigator
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.setupWithNavController
import com.wsiiz.projekt.przyroda_klasa_4.R
import com.wsiiz.projekt.przyroda_klasa_4.base.activity.BaseActivity
import com.wsiiz.projekt.przyroda_klasa_4.databinding.MainActivityLayoutBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : BaseActivity<MainActivityViewModel>() {

    private lateinit var binding: MainActivityLayoutBinding
    override val viewModel: MainActivityViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.main_activity_layout)
        binding.setVariable(BR.viewModel, viewModel)

        binding.bottomNavigationView.setupWithNavController(getNavController())
        binding.bottomNavigationView.setOnNavigationItemReselectedListener { }
    }

    private fun getNavController(): NavController {
        val navigationHost: NavHostFragment =
            supportFragmentManager.findFragmentById(R.id.fragmentContainer) as NavHostFragment
        return navigationHost.navController
    }

    fun navigateTo(navDirections: NavDirections) {
// Imperatywne:       supportFragmentManager.beginTransaction().add(Fragment).commit()
        getNavController().navigate(navDirections)
    }

    fun navigateTo(navDirections: NavDirections, extras: FragmentNavigator.Extras) {
        getNavController().navigate(navDirections, extras)
    }

    fun navigateUp() {
        getNavController().navigateUp()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        val hostFragment = supportFragmentManager.fragments[0] as NavHostFragment
        for (fragment in hostFragment.childFragmentManager.fragments) {
            fragment.onActivityResult(requestCode, resultCode, data)
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        val hostFragment = supportFragmentManager.fragments[0] as NavHostFragment
        for (fragment in hostFragment.childFragmentManager.fragments) {
            fragment.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }
}