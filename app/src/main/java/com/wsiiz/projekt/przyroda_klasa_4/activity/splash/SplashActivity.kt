package com.wsiiz.projekt.przyroda_klasa_4.activity.splash

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.wsiiz.projekt.przyroda_klasa_4.activity.login.LoginActivity
import com.wsiiz.projekt.przyroda_klasa_4.activity.main.MainActivity
import com.wsiiz.projekt.przyroda_klasa_4.data.shared_preferences.SharedPreferences
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class SplashActivity : AppCompatActivity() {

    @Inject
    lateinit var sharedPreferences: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val token = sharedPreferences.getToken()
        val userId = sharedPreferences.getUserId()

        if (token.isEmpty() || userId == -1) {
            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
        } else {
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }
    }
}