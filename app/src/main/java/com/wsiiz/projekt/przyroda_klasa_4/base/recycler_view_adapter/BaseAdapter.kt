package com.wsiiz.projekt.przyroda_klasa_4.base.recycler_view_adapter

import androidx.recyclerview.widget.RecyclerView

abstract class BaseAdapter<VH: RecyclerView.ViewHolder> : RecyclerView.Adapter<VH>() {

}