package com.wsiiz.projekt.przyroda_klasa_4.activity.login

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil
import androidx.databinding.library.baseAdapters.BR
import androidx.lifecycle.Observer
import com.wsiiz.projekt.przyroda_klasa_4.R
import com.wsiiz.projekt.przyroda_klasa_4.activity.main.MainActivity
import com.wsiiz.projekt.przyroda_klasa_4.base.activity.BaseActivity
import com.wsiiz.projekt.przyroda_klasa_4.databinding.LoginActivityLayoutBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class LoginActivity : BaseActivity<LoginViewModel>() {

    lateinit var binding: LoginActivityLayoutBinding
    override val viewModel: LoginViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.login_activity_layout)
        binding.lifecycleOwner = this
        binding.setVariable(BR.viewModel, viewModel)

        observeLoginState()
    }

    private fun observeLoginState() {
        viewModel.state.observe(this, Observer { state ->
            when (state) {
                LoginViewModel.State.ERROR -> {
                    Toast.makeText(applicationContext, R.string.error, Toast.LENGTH_LONG).show()
                }
                LoginViewModel.State.SUCCESS -> {
                    val intent = Intent(this, MainActivity::class.java)
                    startActivity(intent)
                }
                else -> return@Observer
            }
        })
    }
}