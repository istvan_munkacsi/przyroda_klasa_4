package com.wsiiz.projekt.przyroda_klasa_4.fragments.one_article

import android.os.Bundle
import android.transition.TransitionInflater
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.library.baseAdapters.BR
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import com.wsiiz.projekt.przyroda_klasa_4.R
import com.wsiiz.projekt.przyroda_klasa_4.base.fragment.BaseFragment
import com.wsiiz.projekt.przyroda_klasa_4.data.shared_preferences.SharedPreferences
import com.wsiiz.projekt.przyroda_klasa_4.databinding.OneArticleFragmentLayoutBinding
import javax.inject.Inject

class OneArticleFragment : BaseFragment<OneArticleViewModel>() {

    @Inject
    lateinit var sp: SharedPreferences

    private lateinit var binding: OneArticleFragmentLayoutBinding
    override val viewModel: OneArticleViewModel by viewModels()

    private val navArguments: OneArticleFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.one_article_fragment_layout, container, false)
        binding.lifecycleOwner = this
        binding.setVariable(BR.viewModel, viewModel)
        binding.setVariable(BR.article, navArguments.article)
        return binding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        sharedElementEnterTransition =
            TransitionInflater.from(context).inflateTransition(android.R.transition.move)
    }
}