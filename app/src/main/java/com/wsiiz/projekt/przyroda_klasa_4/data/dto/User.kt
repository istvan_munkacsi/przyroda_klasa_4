package com.wsiiz.projekt.przyroda_klasa_4.data.dto

data class User(
    val id: Int = -1,
    val name: String?
)