package com.wsiiz.projekt.przyroda_klasa_4.fragments.one_article

import androidx.lifecycle.MutableLiveData
import com.wsiiz.projekt.przyroda_klasa_4.base.view_model.BaseViewModel

class OneArticleViewModel : BaseViewModel<Unit>() {

    override val state: MutableLiveData<Unit>
        get() = MutableLiveData()
}